package edu.ntnu.stud.trains;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a trainstation.
 */
public class TrainStation {
  private final ArrayList<TrainDeparture> departures;
  private LocalTime stationClock;

  /**
   * Constructor for TrainStation.
   *
   * @param stationClock representation of the station time when the station is initiated
   */
  public TrainStation(String stationClock) throws IllegalArgumentException {
    this.departures = new ArrayList<>();
    setStationClock(stationClock);
  }

  /**
   * Accessor method for the registered departures in trainstation.
   *
   * @return List of registered departures
   */
  public List<TrainDeparture> getDepartures() {
    return departures;
  }

  /**
   * Adds a departure to the list of departures.
   *
   * @param departure Departure to add
   */
  public void addDeparture(TrainDeparture departure) {
    if (departure == null) {
      throw new IllegalArgumentException("Departure cannot be empty");
    } else {
      this.departures.add(departure);
    }
  }

  /**
   * Returns the station clock.
   *
   * @param stationClock Station clock
   * @throws IllegalArgumentException If string is not parseable to LocalTime
   */
  public void setStationClock(String stationClock) {
    try {
      this.stationClock = LocalTime.parse(stationClock, DateTimeFormatter.ofPattern("HH:mm"));
    } catch (DateTimeParseException e) {
      throw new IllegalArgumentException("Invalid time format for station clock: '" + stationClock
              + "'. Please use the format 'hh:mm'.");
    }
  }

  /**
   * Accessor method for the stations time.
   *
   * @return Station clock
   */
  public LocalTime getStationClock() {
    return stationClock;
  }

  /**
   * Checks if the train number is already in use.
   *
   * @param trainNumber Train number to filter by
   * @return returns departure if train number is already in use,
   *         if else returns null
   */
  public TrainDeparture trainNumberStream(int trainNumber) {
    return this.departures.stream().filter(departure -> departure.getTrainNumber() == trainNumber)
            .findFirst().orElse(null);
  }

  /**
   * Filters through the registered departures and
   * returns departure if line and departure time matches.
   *
   * @param line Line of the train departure
   * @param departureTime departure time of the train departure to filter by
   * @return returns departure if line and departure time matches or else returns null
   */
  public TrainDeparture trainLineStream(String line, LocalTime departureTime) {
    return this.departures.stream().filter(departure -> departure.getLine().equalsIgnoreCase(line)
            && departure.getNewTime().equals(departureTime)).findFirst().orElse(null);
  }

  /**
   * Filters through the registered departures and
   * returns list of departures if destination matches.
   *
   * @param destination Destination of the train departure to filter by
   * @return list of departures if destination matches
   */
  public List<TrainDeparture> trainDestinationStream(String destination) {
    return this.departures.stream().filter(departure -> departure
            .getDestination().equalsIgnoreCase(destination)).toList();
  }

  /**
   * Filters through the registered departures and
   * returns departure if track and departure time matches.
   *
   * @param track Track of the train departure to filter by
   * @param departureTime Departure time of the train departure to filter by
   * @param trainNumber Train number of the train departure to prevent from being filtered
   * @return returns departure if track and departure time matches or else returns null
   */
  public TrainDeparture trainTrackStream(int track, LocalTime departureTime, int trainNumber) {
    return this.departures.stream().filter(departure -> departure.getTrack()
            == track && departure.getTrack() != -1
            && departure.getNewTime().equals(departureTime)
            && departure.getTrainNumber() != trainNumber).findFirst().orElse(null);
  }

  @Override
  public String toString() {
    String header = "| Departure | Line | Train number "
            + "|     Destination     | Delay (new departure) | Track |";
    int lineLength = header.length();
    String appLogo = "Train Dispatch App ";
    String stationTime = " Station clock: " + this.stationClock;

    int topLineLength = lineLength - (appLogo.length() + stationTime.length());

    return "\n" + appLogo + "-".repeat(topLineLength)
            + stationTime
            + "\n" + "-".repeat(lineLength)
            + "\n" + header
            + "\n" + "-".repeat(lineLength);
  }
}
