package edu.ntnu.stud.trains;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * This class represents a train departure.
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private LocalTime newTime;
  private LocalTime delay = LocalTime.parse("00:00");

  /**
   * Constructor for TrainDeparture when track is not defined. Delay is set to 00:00.
   *
   * @param departureTime Time of departure
   * @param line Line of departure
   * @param trainNumber Train number
   * @param destination Destination
   * @param track Track of departure
   * @throws IllegalArgumentException If any of the arguments are invalid
   */
  public TrainDeparture(String departureTime, String line,
                        int trainNumber, String destination,
                        int track) throws IllegalArgumentException {

    this.departureTime = setDepartureTime(departureTime);
    this.line = setLine(line);
    this.trainNumber = setTrainNumber(trainNumber);
    this.destination = setDestination(destination);
    setTrack(track);
    this.newTime = this.departureTime;
  }

  /**
   * Constructor for TrainDeparture when track is not defined. Delay is set to 00:00.
   *
   * @param departureTime Time of departure
   * @param line Line of departure
   * @param trainNumber Train number
   * @param destination Destination
   * @throws IllegalArgumentException If any of the arguments are invalid
   */
  public TrainDeparture(String departureTime, String line,
                        int trainNumber, String destination)
          throws IllegalArgumentException, DateTimeParseException {

    this.departureTime = setDepartureTime(departureTime);
    this.line = setLine(line);
    this.trainNumber = setTrainNumber(trainNumber);
    this.destination = setDestination(destination);
    this.track = -1;
    this.newTime = this.departureTime;
  }

  /**
   * Accessor method for departure time.
   *
   * @return time of departure
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Accessor method for line.
   *
   * @return line of departure
   */
  public String getLine() {
    return line;
  }

  /**
   * Accessor method for train number.
   *
   * @return train number
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Accessor method for destination.
   *
   * @return destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Accessor method for track.
   *
   * @return track
   */
  public int getTrack() {
    return track;
  }

  /**
   * Accessor method for newTime.
   *
   * @return delay
   */
  public LocalTime getNewTime() {
    return newTime;
  }

  /**
   * Private set method for departure time. Throws exception if the time format is not correct.
   *
   * @param departureTime String for departure time
   * @return parameter parsed to LocalTime
   * @throws IllegalArgumentException If the departure time is not parseable to LocalTime
   */
  private LocalTime setDepartureTime(String departureTime)
          throws IllegalArgumentException {
    try {
      return LocalTime.parse(departureTime, DateTimeFormatter.ofPattern("HH:mm"));
    } catch (DateTimeParseException e) {
      throw new IllegalArgumentException(
              "Invalid departure time format: '" + departureTime
                      + "'. Please use the format 'HH:mm'.");
    }
  }

  /**
   * Private set method for line. Throws exception if the line is blank or longer than 4 characters.
   *
   * @param line String for line
   * @return parameter
   * @throws IllegalArgumentException If the line is blank or longer than 4 characters
   *
   */
  private String setLine(String line) throws IllegalArgumentException {
    if (line.isBlank()) {
      throw new IllegalArgumentException("Departure must have a line");
    }
    if (line.length() > 4) {
      throw new IllegalArgumentException("Line must be less than 4 characters");
    }
    return line;
  }

  /**
   * Private set method for train number. Throws exception if the train number is not a positive
   *
   * @param trainNumber Integer for train number
   * @return parameter
   * @throws IllegalArgumentException If the train number is not a positive integer
   *                                  or has more digits than 9
   */
  private int setTrainNumber(int trainNumber) throws IllegalArgumentException {
    if (trainNumber <= 0) {
      throw new IllegalArgumentException("Train number must be a positive integer");
    }
    if (Integer.toString(trainNumber).length() > 9) {
      throw new IllegalArgumentException("Train number must be a positive integer,"
              + " with less than 9 digits");
    }
    return trainNumber;
  }

  /**
   * Private set method for destination used in constructors.
   *
   * @param destination String for destination
   * @return parameter
   * @throws IllegalArgumentException If the destination is blank or longer than 20 characters
   */
  private String setDestination(String destination) throws IllegalArgumentException {
    if (destination.isBlank()) {
      throw new IllegalArgumentException("Departure must have a destination");
    }
    if (destination.length() > 20) {
      throw new IllegalArgumentException("Destination must be less than 20 characters");
    }
    return destination;
  }

  /**
   * Sets the track of the train departure.
   *
   * @param track The track of the train departure
   */
  public void setTrack(int track) {
    if (track > 15) {
      throw new IllegalArgumentException("There are only 15 tracks available");
    }
    if (track < -1) {
      throw new IllegalArgumentException("Track must be a positive integer");
    }
    if (track == 0) {
      this.track = -1;
    } else {
      this.track = track;
    }
  }

  /**
   * Sets the delay of the train departure.
   *
   * @param delay The delay of the train departure
   */

  public void setDelay(String delay) {
    try {
      LocalTime newDelay = this.delay.plusHours(LocalTime.parse(delay).getHour())
              .plusMinutes(LocalTime.parse(delay).getMinute());
      LocalTime delayedTime = this.departureTime.plusHours(
              newDelay.getHour()).plusMinutes(newDelay.getMinute());
      if (delayedTime.isBefore(this.departureTime)) {
        throw new IllegalArgumentException(
                "Delay can not make the train depart the next day");
      } else {
        this.delay = newDelay;
        this.newTime = delayedTime;
      }
    } catch (DateTimeParseException e) {
      throw new IllegalArgumentException(
             "Wrong input: '" + delay + "' is not a valid time. "
                     + "Please use the format 'hh:mm'.");
    }
  }

  /**
   * Prints departures in a table format.
   *
   * @return String with departure information and varies depending on track and delay
   */


  @Override
  public String toString() {
    if (delay.equals(LocalTime.parse("00:00"))) {
      String format = "| %-9s | %-4s | %-12s | %-19s | %-21s | %-5s |";
      if (track == -1) {
        return String.format(
                format, departureTime, line, trainNumber, destination, "", "");
      } else {
        return String.format(
                format, departureTime, line, trainNumber, destination, "", track);
      }
    } else {
      String format = "| %-9s | %-4s | %-12s | %-19s | %-5s ( %-5s )       | %-5s |";
      if (track == -1) {
        return String.format(
                format, departureTime, line, trainNumber, destination, delay, newTime, "");
      } else {
        return String.format(
                format, departureTime, line, trainNumber, destination, delay, newTime, track);

      }
    }
  }
}
