package edu.ntnu.stud.interfaces;

import edu.ntnu.stud.trains.TrainDeparture;
import edu.ntnu.stud.trains.TrainStation;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * This interface contains methods for displaying information to the user.
 */
interface Displays {
  /**
   * Displays the menu.
   */
  default void showMenu() {
    System.out.println(" - Train Departures - ");
    System.out.println("Choose an action: ");
    System.out.println("1| Display Train Departures ");
    System.out.println("2| Add Train Departure ");
    System.out.println("3| Change Track ");
    System.out.println("4| Add Delay ");
    System.out.println("5| Search for Departure (by train number) ");
    System.out.println("6| Search for Departure (by destination) ");
    System.out.println("7| Update the Clock ");
    System.out.println("8| Display previous Departures");
    System.out.println("9| Close application");
  }

  /**
   * Displays all train departures in parameter.
   *
   * @param trainStation list that contains train departures to display
   */
  default void table(TrainStation trainStation, List<TrainDeparture> departures) {

    if (departures.isEmpty()) {
      System.out.println("There are no registered departures \n");
    } else {
      ArrayList<TrainDeparture> sortedDepartures = new ArrayList<>(departures.stream().sorted(
              Comparator.comparing(TrainDeparture::getNewTime)).toList());
      System.out.println(trainStation);
      for (TrainDeparture departure : sortedDepartures) {
        System.out.println(departure);
      }
      int lineLength = trainStation.toString().replace("\n", "").length() / 4;
      System.out.println("-".repeat(lineLength) + "\n"); // 89 is the length of the header
    }
  }

  /**
   * Displays two options to the user: try again or exit.
   *
   * @param scan is needed to read the user input
   * @return true if the user wants to exit the application, false if the user wants to continue
   */
  default boolean continueOrExit(Scanner scan) {
    System.out.println("Try again | Press any key (except 9) + Enter |");
    System.out.println("Exit without saving | Press 9 + Enter |");
    return scan.nextLine().equals("9");
  }
}
