package edu.ntnu.stud.interfaces;

import edu.ntnu.stud.trains.TrainDeparture;
import edu.ntnu.stud.trains.TrainStation;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

/**
 * This class contains methods for validating user input for fields in TrainDeparture class.
 */
interface ValidateInput extends Displays {

  /**
   * Asks the user to type in a train number.
   *
   * @param scan is needed to read the user input
   * @return the train number is returned
   */
  default int validateTrainNumber(Scanner scan) {
    boolean validTrainNumber = false;
    int trainNumber = 0;
    while (!validTrainNumber) {
      System.out.println("Type in train number: ");
      while (!scan.hasNextInt()) {
        System.out.println("Invalid input. Please enter a valid integer as train number: ");
        scan.next();
      }

      trainNumber = scan.nextInt();
      scan.nextLine();

      if (trainNumber < 1) {
        System.out.println("Train number can not be negative or 0\n");
      } else if (Integer.toString(trainNumber).length() > 9) {
        System.out.println("Train number can not be more than 9 digits\n");
      } else {
        validTrainNumber = true;
      }
    }
    return trainNumber;
  }

  /**
   * Method that validates the destination.
   *
   * @param scan is needed to read the user input
   * @return A string with the destination and the first letter in each word is uppercase
   */
  default String validateDestination(Scanner scan) {
    boolean validDestination = false;
    StringBuilder updatedDestination = null;
    do {
      System.out.println("Type in destination: ");
      String destination = scan.nextLine();
      if (destination.length() < 2) {
        System.out.println("Destination must be more than 1 character\n");
      } else if (destination.length() >= 20) {
        System.out.println("Destination must be less than 20 characters\n");
      } else {
        String[] destinationWords = destination.split(" ");
        updatedDestination = new StringBuilder();
        for (String word : destinationWords) {
          if (!word.isEmpty() || !word.isBlank()) {
            // ChatGPT recommended this to be able to make
            // the first letter of each word uppercase and the rest lowercase
            updatedDestination.append(word.substring(0, 1).toUpperCase())
                    .append(word.substring(1).toLowerCase())
                    .append(" ");
          }
        }
        validDestination = true;
      }
    } while (!validDestination);
    return updatedDestination.toString().trim();
    //trim() removes the last space and other unnecessary spaces
  }

  /**
   * Method that validates the line.
   *
   * @param scan is needed to read the user input
   * @return A string with the line and all letters are uppercase
   */
  default String validateLine(Scanner scan) {
    String line = "";
    boolean validLine = false;
    while (!validLine) {
      System.out.println("Type in line: ");
      line = scan.nextLine().trim().toUpperCase();
      if (line.isBlank()) {
        System.out.println("Line can not be empty\n");
      } else if (line.length() > 4) {
        System.out.println("Line can not be more than 4 characters long\n");
      } else {
        validLine = true;
      }
    }
    return line;
  }

  /**
   * Method that validates the departure time.
   *
   * @param scan is needed to read the user input
   * @param trainStation is needed to filter through registered departures
   * @param line is needed to check if there are any trains with the same
   *             line that departs at the same time
   * @return A string with the departure time or null if the user wants to exit
   */
  default String validateDepartureTime(Scanner scan,
                                       TrainStation trainStation,
                                       String line) {
    boolean validTime = false;
    String departureString = "";
    LocalTime departureTime;
    TrainDeparture train;
    while (!validTime) {
      try {
        System.out.println("Type in departure time 'hh:mm': ");
        departureString = scan.nextLine();
        departureTime = LocalTime.parse(departureString, DateTimeFormatter.ofPattern("HH:mm"));
        if (departureTime.isBefore(trainStation.getStationClock())
                || departureTime.equals(trainStation.getStationClock())) {
          System.out.println("The departure can not be before or at the current station time\n");
          validTime = continueOrExit(scan);
          if (validTime) {
            return null;
          }
        } else {
          train = trainStation.trainLineStream(line, departureTime);
          if (train != null) {
            table(trainStation, List.of(train));
            System.out.println("""
                    The train above has the same line and is set to the selected departure time.
                    Please choose another time or exit without saving:
                    """);
            validTime = continueOrExit(scan);
            if (validTime) {
              return null;
            }
          } else {
            validTime = true;
          }
        }
      } catch (Exception e) {
        System.out.println("Wrong input: '" + departureString + "' is not a valid time\n");
        validTime = continueOrExit(scan);
        if (validTime) {
          return null;
        }
      }
    }
    return departureString;
  }

  /**
   * Method that validates the track.
   *
   * @param scan is needed to read the user input
   * @param trainStation is needed to filter through registered departures
   * @param departureTime is needed to check if there are any trains with the same
   *                      departure time that departs from the same track
   * @param trainNumber is needed for the trainTrackStream method to filter
   *                   through registered departures except itself
   * @return An integer with the track
   */
  default int validateTrack(Scanner scan,
                            TrainStation trainStation,
                            LocalTime departureTime,
                            int trainNumber) {
    boolean validTrack = false;
    int track = 0;
    String trackString = null;
    while (!validTrack) {
      try {
        System.out.println("Type in track or press enter to assign track later: ");
        trackString = scan.nextLine();
        if (trackString.isEmpty() || trackString.isBlank()) {
          return -1;
        } else {
          track = Integer.parseInt(trackString);
          if (track < 0) {
            System.out.println("Track must be a positive integer\n");
          } else if (track > 15) {
            System.out.println("There are only 15 tracks at the trainstation\n");
          } else {
            TrainDeparture train = trainStation.trainTrackStream(track, departureTime, trainNumber);
            if (train != null) {
              table(trainStation, List.of(train));
              System.out.println("""
                    The train above is set to depart on the selected track at the same time.
                    Please choose another track or press enter to assign track later
                    """);
            } else {
              validTrack = true;
            }
          }
        }
      } catch (Exception n) {
        System.out.println("Can not set track to: '" + trackString
                + "'" + ". Type in a positive integer:\n");
      }
    }
    return track;
  }
}
