package edu.ntnu.stud.interfaces;

import edu.ntnu.stud.trains.TrainDeparture;
import edu.ntnu.stud.trains.TrainStation;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class represents the user interface for the train dispatch application.
 */
public class UserInterface implements ValidateInput {
  private final Scanner scan = new Scanner(System.in);

  private enum Options {
    DISPLAY_DEPARTURES,
    ADD_DEPARTURE,
    CHANGE_TRACK,
    ADD_DELAY,
    SEARCH_TRAIN_NUMBER,
    SEARCH_DESTINATION,
    UPDATE_STATION_CLOCK,
    DISPLAY_PREVIOUS_DEPARTURES,
    EXIT,
    DEFAULT

  }

  //GitHub Copilot helped complete JavaDoc for this class
  /**
   * Initializes the train station with a time for the station clock.
   *
   * @return The initial train station is returned so that it can be used in the main method
   */
  public TrainStation init() {
    return new TrainStation("06:00");
  }

  /**
   * Starts the user interface.
   *
   * @param trainStation The train station to operate on
   */
  public void start(TrainStation trainStation) {
    TrainDeparture trainDeparture = new TrainDeparture("12:00", "L1", 1, "Oslo", 1);
    TrainDeparture trainDeparture2 = new TrainDeparture("12:15", "L1", 2, "Trondheim");
    TrainDeparture trainDeparture3 = new TrainDeparture("12:30", "F4", 3, "Bergen", 2);
    trainStation.addDeparture(trainDeparture);
    trainStation.addDeparture(trainDeparture2);
    trainStation.addDeparture(trainDeparture3);
    displayDepartures(trainStation, trainStation.getDepartures());

    int selectionInt = 0;
    while (selectionInt != 9) {
      String selectionString = "";
      showMenu();
      try {
        selectionString = scan.nextLine();
        selectionInt = Integer.parseInt(selectionString);
        Options option;
        if (selectionInt < 1 || selectionInt > 9) {
          option = Options.DEFAULT;
        } else {
          option = Options.values()[selectionInt - 1];
        }

        switch (option) {
          case DISPLAY_DEPARTURES -> displayDepartures(trainStation, trainStation.getDepartures());
          case ADD_DEPARTURE -> newDeparture(trainStation);
          case CHANGE_TRACK -> changeTrack(trainStation);
          case ADD_DELAY -> addDelay(trainStation);
          case SEARCH_TRAIN_NUMBER -> searchTrainNumber(trainStation);
          case SEARCH_DESTINATION -> searchDestination(trainStation);
          case UPDATE_STATION_CLOCK -> updateStationClock(trainStation);
          case DISPLAY_PREVIOUS_DEPARTURES -> displayPreviousDepartures(
                  trainStation, trainStation.getDepartures());
          case EXIT -> endApplication();
          default -> System.out.println("Invalid selection: " + "'" + selectionInt + "'"
                  + ", write a number between 1 and 9");
        }
      } catch (Exception n) {
        System.out.println("Write a number between 1 and 9, not: " + "'" + selectionString + "'");
      }
    }
  }

  /**
   * display a message when the application is closed.
   */
  private void endApplication() {
    System.out.println(" - Application closed - ");
    scan.close();
  }

  /**
   * display the train departures in a table.
   *
   * @param trainStation is needed because of the toString() method in TrainStation
   * @param departures the list of departures to display
   */
  private void displayDepartures(TrainStation trainStation,
                                        List<TrainDeparture> departures) {
    if (departures.isEmpty()) {
      System.out.println("There are currently no registered train departures \n");
    } else {

      //toString() method in TrainStation
      LocalTime stationClock = trainStation.getStationClock();
      ArrayList<TrainDeparture> comingDepartures = new ArrayList<>();
      for (TrainDeparture departure : departures) {
        if (departure.getNewTime().isAfter(stationClock)) {
          //toString() method in TrainDeparture
          comingDepartures.add(departure);
        }
      }
      table(trainStation, comingDepartures);
    }
  }

  /**
   * Creates a new train departure.
   *
   * @param trainStation in which we add the new departure to
   */
  private void newDeparture(TrainStation trainStation) {
    boolean endInput;
    try {
      do {
        int trainNumber = validateTrainNumber(scan);
        TrainDeparture train = trainStation.trainNumberStream(trainNumber);
        if (train != null) {
          System.out.println("""
              Train number is already registered
              Please choose a unique train number or exit without saving:
              """);
          endInput = continueOrExit(scan);
        } else {
          String destination = validateDestination(scan);
          String line = validateLine(scan);
          String departureString = validateDepartureTime(scan, trainStation, line);
          LocalTime departureTime;
          if (departureString != null) {
            departureTime = LocalTime.parse(departureString);

            int track = validateTrack(scan, trainStation, departureTime, trainNumber);
            TrainDeparture newDeparture;
            if (track == -1) {
              newDeparture = new TrainDeparture(departureString, line, trainNumber, destination);
            } else {
              newDeparture = new TrainDeparture(departureString, line, trainNumber,
                      destination, track);
            }
            trainStation.addDeparture(newDeparture);
            displayDepartures(trainStation, List.of(newDeparture));
          }
          endInput = true;
        }
      } while (!endInput);
    } catch (NumberFormatException e) {
      System.out.println("Invalid input. " + e.getMessage() + ". Please enter a valid integer. ");
    } catch (DateTimeParseException | IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Changes the track of a train departure.
   *
   * @param trainStation contains the train departures in which the user search for departure,
   *                     then change track
   */
  private void changeTrack(TrainStation trainStation) {
    boolean endInput = false;
    while (!endInput) {
      int trainNumber = validateTrainNumber(scan);
      TrainDeparture train = trainStation.trainNumberStream(trainNumber);

      if (train == null || train.getNewTime().isBefore(trainStation.getStationClock())) {
        System.out.println("Train number '" + trainNumber + "' is not registered\n");
        endInput = continueOrExit(scan);
      } else {
        int track = validateTrack(scan, trainStation,
                train.getNewTime(), trainNumber);
        train.setTrack(track);
        displayDepartures(trainStation, List.of(train));
        endInput = true;

      }
    }
  }

  /**
   * Adds a delay to a train departure.
   *
   * @param trainStation contains the train departures which the user can search for departure,
   *                     then add delay
   */
  private void addDelay(TrainStation trainStation) {
    boolean endInput = false;
    boolean validTime = false;
    String delayString = "";
    while (!endInput) {
      int trainNumber = validateTrainNumber(scan);
      TrainDeparture train = trainStation.trainNumberStream(trainNumber);
      if (train == null || train.getNewTime().isBefore(trainStation.getStationClock())) {
        System.out.println("Train number '" + trainNumber + "' is not registered\n");
        endInput = continueOrExit(scan);
      } else {
        while (!validTime) {
          try {
            System.out.println("Type in delay 'hh:mm': ");
            delayString = scan.nextLine();
            LocalTime delay = LocalTime.parse(delayString, DateTimeFormatter.ofPattern("HH:mm"));
            LocalTime newTime = train.getNewTime().plusHours(delay.getHour())
                    .plusMinutes(delay.getMinute());
            TrainDeparture trackFilter = trainStation.trainTrackStream(train.getTrack(), newTime,
                    trainNumber);
            TrainDeparture lineFilter = trainStation.trainLineStream(train.getLine(), newTime);
            if (trackFilter != null) {
              table(trainStation, List.of(trackFilter));
              System.out.println("""
                      The track is already occupied at the selected time.
                      Please choose another time or exit without saving:
                      """);
              validTime = continueOrExit(scan);
              endInput = validTime;
            } else if (lineFilter != null) {
              table(trainStation, List.of(lineFilter));
              System.out.println("""
                      The line is already occupied at the selected time by the departure above.
                      Please choose another time or exit without saving:
                      """);
              validTime = continueOrExit(scan);
              endInput = validTime;
            } else {
              train.setDelay(delayString);
              displayDepartures(trainStation, List.of(train));
              validTime = true;
              endInput = true;
            }
          } catch (DateTimeParseException e) {
            System.out.println("Wrong input: '" + delayString + "' is not a valid time."
                    + " Please use the format 'hh:mm'.\n");
            validTime = continueOrExit(scan);
            endInput = validTime;
          } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            validTime = continueOrExit(scan);
            endInput = validTime;
          }
        }
      }
    }
  }

  /**
   * Searches for a train departure by train number.
   *
   * @param trainStation contains the train departures which the user can search for departure
   *                     by train number
   */
  private void searchTrainNumber(TrainStation trainStation) {
    int trainNumber = validateTrainNumber(scan);
    TrainDeparture train = trainStation.trainNumberStream(trainNumber);
    if (train == null) {
      System.out.println("Train number '" + trainNumber + "' is not registered\n");
    } else {
      displayDepartures(trainStation, List.of(train));
    }
  }

  /**
   * Searches for a train departure by destination.
   *
   * @param trainStation contains the train departures which the user can search for departure
   *                     by destination
   */
  private void searchDestination(TrainStation trainStation) {
    String destination = validateDestination(scan);
    List<TrainDeparture> trains = trainStation.trainDestinationStream(destination);
    if (trains.isEmpty()) {
      System.out.println("No trains to " + destination + " is registered");
    } else {
      displayDepartures(trainStation, trains);
    }
  }

  /**
   * Updates the station clock. The user can not set the time to a time before the current time.
   *
   * @param trainStation instance of TrainStation where the station clock is updated
   */
  private void updateStationClock(TrainStation trainStation) {
    boolean endInput = false;
    while (!endInput) {
      System.out.println("Type in the time: ");
      String time = scan.nextLine();
      try {
        if (LocalTime.parse(time).isBefore(trainStation.getStationClock())) {
          System.out.println("The time can not be before the current time");
          endInput = continueOrExit(scan);
        } else {
          trainStation.setStationClock(time);
          displayDepartures(trainStation, trainStation.getDepartures());
          endInput = true;
        }

      } catch (IllegalArgumentException e) {
        System.out.println("Wrong input: '" + time + "' is not a valid time. "
                + "Please use the format 'hh:mm'.");
      }
    }
  }

  private void displayPreviousDepartures(TrainStation trainStation,
                                        List<TrainDeparture> departures) {
    LocalTime stationClock = trainStation.getStationClock();
    ArrayList<TrainDeparture> previousDepartures = new ArrayList<>();
    for (TrainDeparture departure : departures) {
      LocalTime departureTimePlusDelay = departure.getNewTime();
      if (departureTimePlusDelay.isBefore(stationClock)
              || departureTimePlusDelay.equals(stationClock)) {
        //toString() method in TrainDeparture
        previousDepartures.add(departure);
      }
    }
    if (previousDepartures.isEmpty()) {
      System.out.println("There are no previously registered train departures \n");
    } else {
      table(trainStation, previousDepartures);
    }
  }
}
