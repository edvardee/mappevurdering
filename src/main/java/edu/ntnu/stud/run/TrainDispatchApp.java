package edu.ntnu.stud.run;

import edu.ntnu.stud.interfaces.UserInterface;
import edu.ntnu.stud.trains.TrainStation;


/**
 * This is the main class for the train dispatch application.
 */
public class TrainDispatchApp {

  /**
   * The main method. Which creates a new UserInterface object
   * and calls the init and start methods.
   *
   * @param args The command line arguments which in this case is not used.
   */
  public static void main(String[] args) {
    UserInterface userInterface = new UserInterface();
    TrainStation trainStation = userInterface.init();
    userInterface.start(trainStation);
  }
}
