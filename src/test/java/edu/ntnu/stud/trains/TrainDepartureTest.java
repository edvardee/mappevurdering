package edu.ntnu.stud.trains;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Test class for the class TrainDeparture.
 */
class TrainDepartureTest {

  /**
   * Test TrainDeparture constructors and accessor methods.
   */
  @Nested
  @DisplayName("Test TrainDeparture constructor")
  class ConstructorTests {

    //Positives
    @Test
    @DisplayName("Test first constructor with valid arguments")
    void firstConstructorPositive() {
      TrainDeparture trainDeparture = new TrainDeparture("12:00", "L1", 1, "Oslo");
      try {
        assertEquals(LocalTime.parse("12:00"), trainDeparture.getDepartureTime());
        assertEquals("L1", trainDeparture.getLine());
        assertEquals(1, trainDeparture.getTrainNumber());
        assertEquals("Oslo", trainDeparture.getDestination());
        assertEquals(-1, trainDeparture.getTrack());
      } catch (IllegalArgumentException e) {
        fail("IllegalArgumentException should not be thrown");
      }
    }

    @Test
    @DisplayName("Test second constructor with valid arguments")
    void secondConstructorPositive() {
      TrainDeparture trainDeparture2 = new TrainDeparture("12:00", "L1", 1, "Oslo", 1);
      assertEquals(LocalTime.parse("12:00"), trainDeparture2.getDepartureTime());
      assertEquals("L1", trainDeparture2.getLine());
      assertEquals(1, trainDeparture2.getTrainNumber());
      assertEquals("Oslo", trainDeparture2.getDestination());
      assertEquals(1, trainDeparture2.getTrack());
    }

    //Negatives
    @Test
    @DisplayName("Test first constructor with invalid arguments")
    void firstConstructorNegative() {
      //Test invalid departure time
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("25:00", "L1", 1, "Oslo", 1));

      //Test invalid destination
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "L1", 1, "", 1));
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "L1", 1, "Queen Elizabeth Islands", 1));

      //Test invalid line
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "", 1, "Oslo", 1));
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "L2345", 1, "Oslo", 1));
      //Test invalid train number
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "L1", -1, "Oslo", 1));
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "L1", 1111111111, "Oslo", 1));

      //Test invalid track
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "L1", 1, "Oslo", -3));
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "L1", 1, "Oslo", 16));

    }

    @Test
    @DisplayName("Test second constructor with invalid arguments")
    void secondConstructorNegative() {
      //Arrange, act & assert is included in one test
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("25:00", "L1", 1, "Oslo"));
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "L1", 1, ""));
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "", 1, "Oslo"));
      assertThrows(IllegalArgumentException.class, () ->
              new TrainDeparture("12:00", "L1", -1, "Oslo"));
    }
  }

  /**
   * Test TrainDeparture mutator methods.
   */
  @Nested
  @DisplayName("Test TrainDeparture methods")
  class MutatorTests {


    @Test
    @DisplayName("Positive tests of set methods")
    void setPositive() {
      //Arrange
      TrainDeparture trainDeparture = new TrainDeparture("12:00", "L1", 1, "Oslo");
      TrainDeparture trainDeparture2 = new TrainDeparture("12:00", "L1", 2, "Oslo", 1);
      TrainDeparture trainDeparture3 = new TrainDeparture("12:00", "L1", 2, "Oslo", 1);
      //Act
      trainDeparture.setTrack(1);
      trainDeparture2.setTrack(-1);
      trainDeparture3.setTrack(0);
      trainDeparture.setDelay("01:00");
      //Assert
      assertEquals(1, trainDeparture.getTrack());
      assertEquals(-1, trainDeparture2.getTrack());
      assertEquals(-1, trainDeparture3.getTrack());
      assertEquals(LocalTime.parse("13:00"), trainDeparture.getNewTime());
    }

    @Test
    @DisplayName("Negative tests of set methods")
    void setNegative() {
      //Arrange
      TrainDeparture trainDeparture = new TrainDeparture("12:00", "L1", 1, "Oslo");
      //Act & Assert
      assertThrows(IllegalArgumentException.class, () -> trainDeparture.setTrack(-2));
      assertThrows(IllegalArgumentException.class, () -> trainDeparture.setTrack(16));
      assertThrows(IllegalArgumentException.class, () -> trainDeparture.setDelay("25:00"));
      assertThrows(IllegalArgumentException.class, () -> trainDeparture.setDelay("14:00"));
    }
  }
}