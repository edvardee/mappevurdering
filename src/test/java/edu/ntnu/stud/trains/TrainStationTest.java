package edu.ntnu.stud.trains;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


/**
 * Test class for TrainStation.
 */
class TrainStationTest {

  @Nested
  @DisplayName("Test TrainStation constructor")
  class ConstructorTests {
    @Test
    @DisplayName("Test constructor with valid arguments")
    void constructorValid() {
      TrainStation trainStation = new TrainStation("08:00");
      try {
        assertEquals(LocalTime.parse("08:00"), trainStation.getStationClock());
        assertEquals(0, trainStation.getDepartures().size());
      } catch (IllegalArgumentException e) {
        fail("IllegalArgumentException should not be thrown");
      }
    }

    @Test
    @DisplayName("Test constructor with invalid arguments")
    void constructorInvalidInstances() {
      //Test invalid station clock
      assertThrows(IllegalArgumentException.class, () ->
              new TrainStation(""));
    }
  }

  /**
   * Test TrainStation mutator methods.
   */
  @Nested
  @DisplayName("Test TrainStation methods")
  class MutatorTests {
    @Test
    @DisplayName("Test setStationClock with valid arguments")
    void testSetStationClock() {
      TrainStation trainStation = new TrainStation("08:00");
      try {
        trainStation.setStationClock("12:00");
        assertEquals("12:00", trainStation.getStationClock().toString());
      } catch (IllegalArgumentException e) {
        fail("IllegalArgumentException should not be thrown");
      }
    }

    @Test
    @DisplayName("Test setStationClock with invalid arguments")
    void testSetStationClockInvalidInstances() {
      TrainStation trainStation = new TrainStation("08:00");
      //Test invalid station clock
      assertThrows(IllegalArgumentException.class, () ->
              trainStation.setStationClock("25:00"));
    }

    @Test
    @DisplayName("Test addDeparture with valid arguments")
    void testAddDeparture() {
      TrainStation trainStation = new TrainStation("08:00");
      assertEquals(0, trainStation.getDepartures().size());
      TrainDeparture trainDeparture = new TrainDeparture("12:00", "L1", 1, "Oslo", 1);
      trainStation.addDeparture(trainDeparture);
      assertEquals(1, trainStation.getDepartures().size());
    }

    @Test
    @DisplayName("Test addDeparture with invalid arguments")
    void testAddInvalidDeparture() {
      TrainStation trainStation = new TrainStation("08:00");
      TrainDeparture trainDeparture = new TrainDeparture("12:00", "L1", 1, "Oslo", 1);
      trainStation.addDeparture(trainDeparture);
      assertEquals(1, trainStation.getDepartures().size());
      //Test invalid departure
      assertThrows(IllegalArgumentException.class, () ->
              trainStation.addDeparture(null));
    }
  }

  @Nested
  @DisplayName("Test TrainStation stream methods")
  class StreamsTests {
    @Test
    void testTrainNumberStream() {
      TrainStation trainStation = new TrainStation("08:00");
      TrainDeparture trainDeparture = new TrainDeparture("11:00", "L1", 1, "Oslo", 1);
      TrainDeparture trainDeparture2 = new TrainDeparture("09:00", "L1", 2, "Oslo", 1);
      trainStation.addDeparture(trainDeparture);
      trainStation.addDeparture(trainDeparture2);
      assertEquals(trainDeparture, trainStation.trainNumberStream(1));

    }

    @Test
    void testLineStream() {
      TrainStation trainStation = new TrainStation("08:00");
      TrainDeparture trainDeparture = new TrainDeparture("11:00", "L1", 1, "Oslo", 1);
      TrainDeparture trainDeparture2 = new TrainDeparture("11:00", "L1", 2, "Oslo", 1);
      trainStation.addDeparture(trainDeparture);
      trainStation.addDeparture(trainDeparture2);
      assertEquals(trainDeparture, trainStation.trainLineStream("L1", LocalTime.parse("11:00")));
    }

    @Test
    void testDestinationStream() {
      TrainStation trainStation = new TrainStation("08:00");
      TrainDeparture trainDeparture = new TrainDeparture("11:00", "L1",
              1, "Oslo", 1);
      TrainDeparture trainDeparture2 = new TrainDeparture("12:00", "L1",
              2, "Oslo", 1);
      trainStation.addDeparture(trainDeparture);
      trainStation.addDeparture(trainDeparture2);
      assertEquals(List.of(trainDeparture, trainDeparture2),
              trainStation.trainDestinationStream("Oslo"));
    }

    @Test
    void testTrackStream() {
      TrainStation trainStation = new TrainStation("08:00");
      TrainDeparture trainDeparture = new TrainDeparture("11:00", "L1", 1, "Oslo", 1);
      trainStation.addDeparture(trainDeparture);
      assertEquals(trainDeparture, trainStation.trainTrackStream(1, LocalTime.parse("11:00"), 2));
    }
  }

}