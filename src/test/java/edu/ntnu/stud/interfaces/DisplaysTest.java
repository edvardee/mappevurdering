package edu.ntnu.stud.interfaces;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.stud.trains.TrainDeparture;
import edu.ntnu.stud.trains.TrainStation;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Test class for Displays.
 * Inspiration from user '<b>dfa</b>' on stackoverflow
 * [<a href="https://stackoverflow.com/questions/1119385/junit-test-for-system-out-println/1119559#1119559">...</a>]
 */
class DisplaysTest implements Displays {

  @Nested
  @DisplayName("Test for table method")
  class TableTest {
    private ByteArrayOutputStream outContent;
    private final PrintStream originalOut = System.out;
    private final TrainStation trainStation = new TrainStation("08:00");

    @BeforeEach
    void setUpStreams() {
      outContent = new ByteArrayOutputStream();
      System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void restoreStreams() {
      System.setOut(originalOut);
    }

    /**
     * Test table method with one departure.
     * Here we also get to test the toString in TrainDeparture and TrainStation.
     */
    @Test
    @DisplayName("Test table method")
    void oneDeparture() {
      //Arrange
      TrainDeparture trainDeparture = new TrainDeparture("12:00", "L1", 123, "Oslo");
      String tableOfDeparture = """
                      
          Train Dispatch App ------------------------------------------------- Station clock: 08:00
          -----------------------------------------------------------------------------------------
          | Departure | Line | Train number |     Destination     | Delay (new departure) | Track |
          -----------------------------------------------------------------------------------------
          | 12:00     | L1   | 123          | Oslo                |                       |       |
          -----------------------------------------------------------------------------------------
                      
          """;

      //Act
      table(trainStation, List.of(trainDeparture));

      //Assert
      assertEquals(tableOfDeparture, outContent.toString());
    }

    @Test
    @DisplayName("Test table method with empty list")
    void tableEmptyList() {
      //Arrange
      List<TrainDeparture> nullList = new ArrayList<>();
      String emptyList = "There are no registered departures \n\n";

      //Act
      table(trainStation, nullList);

      //Assert
      assertEquals(emptyList, outContent.toString());
    }
  }

  @Test
  @DisplayName("Test for continueOrExit method")
  void continueOrExit() {
    // Arrange
    Scanner scan = new Scanner(new ByteArrayInputStream("9\n".getBytes()));
    Scanner scan2 = new Scanner(new ByteArrayInputStream("\n".getBytes()));

    // Act
    boolean exit = continueOrExit(scan);
    boolean continued = continueOrExit(scan2);

    // Assert
    assertTrue(exit);
    assertFalse((continued));
  }
}