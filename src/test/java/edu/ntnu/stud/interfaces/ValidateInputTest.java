package edu.ntnu.stud.interfaces;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import edu.ntnu.stud.trains.TrainDeparture;
import edu.ntnu.stud.trains.TrainStation;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalTime;
import java.util.Scanner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Test class for the class ValidateInput.
 * Each negative test has to end with a valid input to avoid NoSuchElementException
 * Inspiration from user '<b>dfa</b>' on stackoverflow
 * [<a href="https://stackoverflow.com/questions/1119385/junit-test-for-system-out-println/1119559#1119559">...</a>]
 * and author '<b>Ana Peterlić</b>' in her article [<a href="https://www.baeldung.com/java-junit-testing-system-in">...</a>]
 */
class ValidateInputTest implements ValidateInput {
  private ByteArrayOutputStream outContent;
  private final PrintStream originalOut = System.out;

  @BeforeEach
  public void setUpStreams() {
    outContent = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outContent));
  }

  @AfterEach
  public void restoreStreams() {
    System.setOut(originalOut);
  }

  @Nested
  @DisplayName("Test validateTrainNumber")
  class ValidateTrainNumber {
    @Test
    @DisplayName("Test train valid input")
    void positive() {
      // Arrange
      Scanner scan = new Scanner(new ByteArrayInputStream("123\n".getBytes()));
      //Act
      int trainNumber = validateTrainNumber(scan);
      //Assert
      assertEquals(123, trainNumber);
    }

    @Nested
    @DisplayName("Train number negatives")
    class Negatives {
      private final String typeIn = "Type in train number: \n";

      @Test
      @DisplayName("Test train number that is not an integer")
      void notInteger() {
        // Arrange
        Scanner scan = new Scanner(new ByteArrayInputStream("f\n9\n".getBytes()));
        String conditionMessage = "Invalid input. Please enter a valid integer as train number: \n";
        String expected = typeIn + conditionMessage;
        // Act
        validateTrainNumber(scan);
        // Assert
        assertEquals(expected, outContent.toString());
      }

      @Test
      @DisplayName("Test train number less than 1")
      void lessThanOne() {
        Scanner scan = new Scanner(new ByteArrayInputStream("0\n9\n".getBytes()));
        validateTrainNumber(scan);
        String conditionMessage = "Train number can not be negative or 0\n\n";
        String expected = typeIn + conditionMessage + typeIn;
        assertEquals(expected, outContent.toString());
      }

      @Test
      @DisplayName("Test train number longer than 9 digits")
      void longerThanNine() {
        Scanner scan = new Scanner(new ByteArrayInputStream("1111111111\n9\n".getBytes()));
        validateTrainNumber(scan);
        String conditionMessage = "Train number can not be more than 9 digits\n\n";
        String expected = typeIn + conditionMessage + typeIn;
        assertEquals(expected, outContent.toString());
      }
    }
  }

  @Nested
  @DisplayName("Test validateDestination")
  class ValidateDestination {

    @Test
    @DisplayName("Test destination positive")
    void positive() {
      // Arrange
      Scanner scan = new Scanner(new ByteArrayInputStream("oslo\n".getBytes()));
      String expected = "Oslo";
      String result = validateDestination(scan);
      assertEquals(expected, result);
    }

    @Nested
    @DisplayName("Destination negatives")
    class Negatives {
      @Test
      @DisplayName("Test destination too many characters")
      void tooLong() {
        Scanner scan = new Scanner(new ByteArrayInputStream(
                "Queen Elizabeth's islands\nOslo\n".getBytes()));
        String typeIn = "Type in destination: \n";
        String conditionMessage = "Destination must be less than 20 characters\n\n";
        String expected = typeIn + conditionMessage + typeIn;
        validateDestination(scan);
        assertEquals(expected, outContent.toString());
      }

      @Test
      @DisplayName("Test destination too short")
      void tooShort() {
        Scanner scan = new Scanner(new ByteArrayInputStream("\nOslo\n".getBytes()));
        String typeIn = "Type in destination: \n";
        String conditionMessage = "Destination must be more than 1 character\n\n";
        String expected = typeIn + conditionMessage + typeIn;
        validateDestination(scan);
        assertEquals(expected, outContent.toString());
      }
    }
  }

  @Nested
  @DisplayName("Test validateLine")
  class ValidateLine {
    @Test
    @DisplayName("Test line positive")
    void positive() {
      // Arrange
      Scanner scan = new Scanner(new ByteArrayInputStream("l3\n".getBytes()));
      String expected = "L3";
      String result = validateLine(scan);
      assertEquals(expected, result);
    }

    @Nested
    @DisplayName("Test line negatives")
    class Negative {

      @Test
      @DisplayName("Test line too many characters")
      void tooLong() {
        Scanner scan = new Scanner(new ByteArrayInputStream("l23455\nOslo\n".getBytes()));
        String typeIn = "Type in line: \n";
        String conditionMessage = "Line can not be more than 4 characters long\n\n";
        String expected = typeIn + conditionMessage + typeIn;
        validateLine(scan);
        assertEquals(expected, outContent.toString());
      }

      @Test
      @DisplayName("Test line too short")
      void tooShort() {
        Scanner scan = new Scanner(new ByteArrayInputStream("\nOslo\n".getBytes()));
        String typeIn = "Type in line: \n";
        String conditionMessage = "Line can not be empty\n\n";
        String expected = typeIn + conditionMessage + typeIn;
        validateLine(scan);
        assertEquals(expected, outContent.toString());
      }

    }
  }

  @Nested
  @DisplayName("Test validateDepartureTime")
  class ValidateDepartureTime {
    private final TrainStation trainStation = new TrainStation("08:00");
    private final String typeIn = "Type in departure time 'hh:mm': \n";
    private final String continueOrExit = """
            Try again | Press any key (except 9) + Enter |
            Exit without saving | Press 9 + Enter |
            """;

    @Test
    @DisplayName("Test departureTime positive")
    void positive() {
      // Arrange
      Scanner scan = new Scanner(new ByteArrayInputStream("12:00\n".getBytes()));
      String expected = "12:00";
      String result = validateDepartureTime(scan, trainStation, "L1");
      assertEquals(expected, result);
    }

    @Nested
    @DisplayName("Test departureTime negatives")
    class Negative {
      @Test
      @DisplayName("Test before station clock")
      void tooEarly() {
        Scanner scan = new Scanner(new ByteArrayInputStream("07:00\n9\n".getBytes()));
        String conditionMessage = """
                The departure can not be before or at the current station time

                """;
        String expected = typeIn + conditionMessage + continueOrExit;
        String result = validateDepartureTime(scan, trainStation, "L1");
        assertEquals(expected, outContent.toString());
        assertNull(result);
      }

      @Test
      @DisplayName("Test departure time with line conflict")
      void lineConflict() {
        TrainDeparture trainDeparture = new TrainDeparture("12:00", "L1", 123, "Oslo");
        trainStation.addDeparture(trainDeparture);
        Scanner scan = new Scanner(new ByteArrayInputStream("12:00\n9\n".getBytes()));
        String conditionMessage = """
         
         Train Dispatch App ------------------------------------------------- Station clock: 08:00
         -----------------------------------------------------------------------------------------
         | Departure | Line | Train number |     Destination     | Delay (new departure) | Track |
         -----------------------------------------------------------------------------------------
         | 12:00     | L1   | 123          | Oslo                |                       |       |
         -----------------------------------------------------------------------------------------
                            
         The train above has the same line and is set to the selected departure time.
         Please choose another time or exit without saving:
         
            """;
        String expected = typeIn + conditionMessage + continueOrExit;
        String result = validateDepartureTime(scan, trainStation, "L1");
        assertEquals(expected, outContent.toString());
        assertNull(result);
      }

      @Test
      @DisplayName("Test not parsable departure time")
      void notParsable() {
        String departureString = "";
        Scanner scan = new Scanner(new ByteArrayInputStream("\n9\n".getBytes()));
        String conditionMessage = "Wrong input: '" + departureString + "' is not a valid time\n\n";
        String expected = typeIn + conditionMessage + continueOrExit;
        String result = validateDepartureTime(scan, trainStation, "L1");
        assertEquals(expected, outContent.toString());
        assertNull(result);
      }
    }
  }

  @Nested
  @DisplayName("Test validateTrack")
  class ValidateTrack {
    private final TrainStation trainStation = new TrainStation("08:00");
    private final String typeIn = "Type in track or press enter to assign track later: \n";

    @Nested
    @DisplayName("Track positives")
    class Positives {

      @Test
      @DisplayName("Test track with input")
      void inputTrack() {
        // Arrange
        Scanner scan = new Scanner(new ByteArrayInputStream("1\n".getBytes()));
        //Act
        int track = validateTrack(scan, trainStation, LocalTime.parse("12:00"), 123);
        //Assert
        assertEquals(1, track);

      }

      @Test
      @DisplayName("Test no assigned track")
      void noTrack() {
        // Arrange
        Scanner scan = new Scanner(new ByteArrayInputStream("\n".getBytes()));
        //Act
        int track = validateTrack(scan, trainStation, LocalTime.parse("12:00"), 123);
        //Assert
        assertEquals(-1, track);
      }

      @Test
      @DisplayName("Test track and departure time conflict")
      void trackConflict() {
        // Arrange
        TrainDeparture trainDeparture = new TrainDeparture("09:00", "L4", 1, "Oslo", 1);
        trainStation.addDeparture(trainDeparture);
        Scanner scan = new Scanner(new ByteArrayInputStream("1\n\n".getBytes()));
        String conditionMessage = """
         
         Train Dispatch App ------------------------------------------------- Station clock: 08:00
         -----------------------------------------------------------------------------------------
         | Departure | Line | Train number |     Destination     | Delay (new departure) | Track |
         -----------------------------------------------------------------------------------------
         | 09:00     | L4   | 1            | Oslo                |                       | 1     |
         -----------------------------------------------------------------------------------------
                            
         The train above is set to depart on the selected track at the same time.
         Please choose another track or press enter to assign track later
         
            """;
        String expected = typeIn + conditionMessage + typeIn;
        //Act
        validateTrack(scan, trainStation, LocalTime.parse("09:00"), 2);
        //Assert
        assertEquals(expected, outContent.toString());
      }
    }

    @Nested
    @DisplayName("Track negatives")
    class Negatives {


      @Test
      @DisplayName("Test track that is not an integer")
      void notInteger() {
        String trackString = "f";
        Scanner scan = new Scanner(new ByteArrayInputStream("f\n9\n".getBytes()));
        validateTrack(scan, trainStation, LocalTime.parse("12:00"), 123);
        String conditionMessage = "Can not set track to: '" + trackString
                + "'. Type in a positive integer:\n\n";
        String expected = typeIn + conditionMessage + typeIn;
        assertEquals(expected, outContent.toString());
      }

      @Test
      @DisplayName("Test track less than 0")
      void lessThanZero() {
        Scanner scan = new Scanner(new ByteArrayInputStream("-1\n5\n".getBytes()));
        validateTrack(scan, trainStation, LocalTime.parse("12:00"), 123);
        String conditionMessage = "Track must be a positive integer\n\n";
        String expected = typeIn + conditionMessage + typeIn;
        assertEquals(expected, outContent.toString());
      }

      @Test
      @DisplayName("Test track bigger than 15")
      void biggerThan15() {
        Scanner scan = new Scanner(new ByteArrayInputStream("20\n5\n".getBytes()));
        validateTrack(scan, trainStation, LocalTime.parse("12:00"), 123);
        String conditionMessage = "There are only 15 tracks at the trainstation\n\n";
        String expected = typeIn + conditionMessage + typeIn;
        assertEquals(expected, outContent.toString());
      }
    }
  }
}