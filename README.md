# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Edvard Berdal Eek"  
STUDENT ID = "559459"

## Project description

[//]: # (TODO: Write a short description of your project/product here.)
This project features a program that contains the departures of a trainstation. 
The program is able to display the train schedule, add new departures, change track of a departure, add delay to a departure, 
search for a departure by destination, and search for a departure by train number, update the station clock and display previous departures.

## Project structure

[//]: # (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)
- `IDATT1003-2023-Mappe-TrainDispatchSystem`
  - `src/` (folder for source files)
    - `main/`
      - `java/`
        - `edu.ntnu.stud/`
          - `interfaces` (package for user interfaces)
            - `Displays.java` (interface for displaying methods)
            - `UserInterface.java` (User interface class)
            - `ValidateInput.java` (interface for user input validation methods)
          - `run` (package for running the program)
            - `TrainDispatchApp.java` (runnable class)
          - `trains` (package for train related classes)
            - `TrainDeparture.java` (train departure class)
            - `TrainStation.java` (train station class)
    - `test/` (folder for JUnit-tests)
        - `java/`
          - `edu.ntnu.stud/`
            - `interfaces` (package for user interfaces)
              - `DisplaysTest.java` (JUnit-test class for displays interface)
              - `ValidateInputTest.java` (JUnit-test class for user input validation interface)
            - `trains` (package for train related classes)
              - `TrainDepartureTest.java` (JUnit-test class for train departure class)
              - `TrainStationTest.java` (JUnit-test class for train station class)
## Link to repository
[//]: # (TODO: Include a link to your repository here.)
 > https://gitlab.stud.idi.ntnu.no/edvardee/mappevurdering

## How to run the project

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)
To run the program, run the main method in the TrainDispatchApp class. 
The program will then run and firstly display three departures in a table, then a menu with different options. 
The user then selects which option they want to use by typing in the number of the option in the terminal. 
The user can then choose to display departures, add new departures, change track of a departure, add delay to a departure. 
Search for a departure by destination, and search for a departure by train number, update the station clock and display previous departures. 
The user will be given instructions on what to type in the terminal to use the different options.

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)

The tests can be run by right-clicking the edu.ntnu.stud package, either within the test or main folder, and selecting "Run 'Tests in edu.ntnu.stud'". 
If you want to run tests with coverage, right-click the edu.ntnu.stud package and select "More Run/Debug" and then "Run 'Tests in edu.ntnu.stud' with Coverage".
The same can be done specific classes within the package. 
## References

[//]: # (TODO: Include references here, if any. For example, if you have used code from the course book, include a reference to the chapter.
Or if you have used code from a website or other source, include a link to the source.)
